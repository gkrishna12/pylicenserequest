import requests, argparse
from dict2xml import dict2xml

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-u",
        "--url",
        dest="url",
        help="URL where request will be sent.",
        required=True
    )
    parser.add_argument(
        "-r",
        "--request",
        dest="request",
        help="Type of request, activation, status or deactivation.",
        required=True
    )
    parser.add_argument(
        "-i",
        "--instance",
        dest="instance",
        help="Random generated instance id for license activation",
        required=True
    )
    parser.add_argument(
        "-e",
        "--email",
        dest="email_id",
        help="Email ID of the customer given while placing the order",
        required=True
    )
    parser.add_argument(
        "-l",
        "--license",
        dest="license_key",
        help="License key generated for the customer and sent to Email ID",
        required=True
    )
    parser.add_argument(
        "-x",
        "--extra",
        dest="extra",
        help="Extra params for logs like IP Address, User Agent, etc.",
        required=True
    )
    args = parser.parse_args()

    payload = {
        'api-request':args.request,
        'instance':args.instance,
        'email':args.email_id,
        'licence_key':args.license_key,
        args.request + '_extra':args.extra
    }

    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'
    }

    r = requests.get(args.url, params=payload, headers=headers)

    response = r.json()
    xml = dict2xml(response,wrap="response",indent="    ",newlines=True)
    print(xml)
    
    file = open("license_response.xml","w+")
    file.write(xml)
    file.close()


if __name__ == '__main__' :
    main()
