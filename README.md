# README #

### What is this repository for? ###

* This repository is for an application that was built as a part of API Communication between a Product's setup application and the License Server.
* The license server is managed by a WordPress plugin named Woocommerce API Manager. The API Manager sends the response in JSON formant where as the Product's Setup Application could accept only XML format.
* Hence this app was developed where the license request is built based on the input given, sends it to the API Manager remote server, gets the response in JSON format and converts it into XML and also writes it to a file.
* Version 1.0.0
* To know more about how the Woocommerce API manager works, [click here](https://docs.woocommerce.com/document/woocommerce-api-manager/).

### How do I get set up? ###

You can run it directly from python in the console. You will have to pass the following arguments to the application:

* -u : URL where request will be sent.
* -r : The API Manager accepts three types of requests : activation, status and deactivation.
* -i : Random generated instance id for license activation. Acts as a unique identifier for each instance of each license key.
* -e : Email ID of the customer given while placing the order.
* -l : License key generated at the time of placing order for the customer which has been sent to customer's registered email id.
* -x : Extra metadata like User Agent, MAC ID, etc., sent along with license request in JSON format.

**Note** : The api endpoints have been modified to remove the product name/id as an argument since it can be determined by the email id and license key provided.

### How to create an application? ###

* To generate an EXE file you need to have pyInstaller installed on your system. Check details [here](http://www.pyinstaller.org/).  
* Go to the folder where you have placed the python file.
* Next step is to generate the exe by running the following command in the command line : 
	pyinstaller --onefile --hidden-import=queue licence-request.py
* You should see an exe file in the Dist folder in the current directory.
* Thats it you're good to go.

### Who do I talk to? ###

* K Gopal Krishna - mail@kgopalkrishna.com 